from typing import Optional
from urllib.parse import urljoin

import requests

from .models import (
    GetActivityParams,
    GetArtistMapParams,
    GetListensParams,
    GetTopEntitiesParams,
)

BASE_URL = "https://api.listenbrainz.org"


def get_listen_count(user_name: str) -> int:
    """Get total number of listens for a user"""

    response = requests.get(url=urljoin(BASE_URL, f"/1/user/{user_name}/listen-count"))
    return int(response.json()["payload"]["count"])


def get_playing_now(user_name: str) -> Optional[dict]:
    """Get the currently playing listen for a user.

    Returns:
        dict: currently playing listen
        None: no listen currently playing
    """

    response = requests.get(url=urljoin(BASE_URL, f"/1/user/{user_name}/playing-now"))
    listens_payload = response.json()["payload"]["listens"]
    if len(listens_payload):
        return listens_payload[0]
    return None


def get_listens(
    user_name: str,
    min_ts: Optional[int] = None,
    max_ts: Optional[int] = None,
    count: Optional[int] = None,
) -> dict:
    """Get the listens for a user"""

    params = GetListensParams(min_ts=min_ts, max_ts=max_ts, count=count)
    response = requests.get(
        url=urljoin(BASE_URL, f"/1/user/{user_name}/listens"), params=params.dict(),
    )
    return response.json()["payload"]


def get_latest_import(user_name: str) -> int:
    """Get the UNIX timestamp of a user's latest import

    Returns:
        int: UNIX timestamp
    """

    response = requests.get(
        url=urljoin(BASE_URL, "/1/latest-import"), params={"user_name": user_name}
    )
    return response.json()["latest_import"]


def get_listening_activity(user_name: str, range_: Optional[str] = None) -> dict:
    """Get the listening activity for user user_name

    The listening activity shows the number of listens the user has submitted
    over a period of time.
    """

    params = GetActivityParams(range=range_)

    response = requests.get(
        url=urljoin(BASE_URL, f"/1/stats/user/{user_name}/listening-activity"),
        params=params.dict(),
    )
    return response.json()["payload"]


def get_daily_activity(user_name: str, range_: Optional[str] = None) -> dict:
    """Get the daily activity for user user_name.

    The daily activity shows the number of listens submitted by the user for
    each hour of the day over a period of time.
    """

    params = GetActivityParams(range=range_)

    response = requests.get(
        url=urljoin(BASE_URL, f"/1/stats/user/{user_name}/daily-activity"),
        params=params.dict(),
    )
    return response.json()["payload"]


def get_top_recordings(
    user_name: str,
    count: Optional[int] = None,
    offset: Optional[int] = None,
    range_: Optional[str] = None,
) -> dict:
    """Get a user's top recordings"""

    params = GetTopEntitiesParams(count=count, offset=offset, range=range_)

    response = requests.get(
        url=urljoin(BASE_URL, f"/1/stats/user/{user_name}/recordings"),
        params=params.dict(),
    )
    return response.json()["payload"]


def get_top_releases(
    user_name: str,
    count: Optional[int] = None,
    offset: Optional[int] = None,
    range_: Optional[str] = None,
) -> dict:
    """Get a user's top releases"""

    params = GetTopEntitiesParams(count=count, offset=offset, range=range_)

    response = requests.get(
        url=urljoin(BASE_URL, f"/1/stats/user/{user_name}/releases"),
        params=params.dict(),
    )
    return response.json()["payload"]


def get_top_artists(
    user_name: str,
    count: Optional[int] = None,
    offset: Optional[int] = None,
    range_: Optional[str] = None,
) -> dict:
    """Get a user's top artists"""

    params = GetTopEntitiesParams(count=count, offset=offset, range=range_)

    response = requests.get(
        url=urljoin(BASE_URL, f"/1/stats/user/{user_name}/artists"),
        params=params.dict(),
    )
    return response.json()["payload"]


def get_artist_map(
    user_name: str,
    range_: Optional[str] = None,
    force_recalculate: Optional[bool] = None,
) -> dict:
    """Get the number of artists the user has listened to from different countries"""

    params = GetArtistMapParams(range=range_, force_recalculate=force_recalculate)

    response = requests.get(
        url=urljoin(BASE_URL, f"/1/stats/user/{user_name}/artist-map"),
        params=params.dict(),
    )
    return response.json()["payload"]
