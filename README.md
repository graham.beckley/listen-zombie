# listen_zombie

A Python wrapper for consuming the [ListenBrainz](listenbrainz.org/) [API](https://listenbrainz.readthedocs.io/en/production/dev/api/)
